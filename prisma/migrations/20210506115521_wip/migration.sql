-- CreateTable
CREATE TABLE "Tag" (
    "id" SERIAL NOT NULL,
    "search" TEXT,
    "comment" TEXT,

    PRIMARY KEY ("id")
);
