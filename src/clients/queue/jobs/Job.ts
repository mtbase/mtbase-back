export enum Job {
  SendEmail = 'sendEmail',
  Hello = 'hello',
  RecalculateStats = 'recalculateStats',

  DbHousekeeping = 'dbHousekeeping',
}
