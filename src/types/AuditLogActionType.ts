// DO NOT EDIT! THIS IS GENERATED FILE

enum AuditLogActionType {
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
}

export default AuditLogActionType;
