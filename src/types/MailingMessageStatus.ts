// DO NOT EDIT! THIS IS GENERATED FILE

enum MailingMessageStatus {
  Draft = 'draft',
  Stopped = 'stopped',
  Pending = 'pending',
  Sent = 'sent',
  Canceled = 'canceled',
  Errored = 'errored',
}

export default MailingMessageStatus;
