import statsBasePermissionToGraphql from './basePermissionsToGraphql';
import {StatsService} from '../../../services/StatsService/StatsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const statsPermissionToGraphql: Partial<PermissionToGraphql<StatsService>> = {
  ...statsBasePermissionToGraphql,
};

export default statsPermissionToGraphql;
