import rolesToPermissionsBasePermissionToGraphql from './basePermissionsToGraphql';
import {RolesToPermissionsService} from '../../../services/RolesToPermissionsService/RolesToPermissionsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const rolesToPermissionsPermissionToGraphql: Partial<PermissionToGraphql<RolesToPermissionsService>> = {
  ...rolesToPermissionsBasePermissionToGraphql,
};

export default rolesToPermissionsPermissionToGraphql;
