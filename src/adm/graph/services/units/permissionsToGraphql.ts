import unitsBasePermissionToGraphql from './basePermissionsToGraphql';
import {UnitsService} from '../../../services/UnitsService/UnitsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const unitsPermissionToGraphql: Partial<PermissionToGraphql<UnitsService>> = {
  ...unitsBasePermissionToGraphql,
};

export default unitsPermissionToGraphql;
