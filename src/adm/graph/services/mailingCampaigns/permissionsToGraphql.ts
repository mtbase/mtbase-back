import mailingCampaignsBasePermissionToGraphql from './basePermissionsToGraphql';
import {MailingCampaignsService} from '../../../services/MailingCampaignsService/MailingCampaignsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const mailingCampaignsPermissionToGraphql: Partial<PermissionToGraphql<MailingCampaignsService>> = {
  ...mailingCampaignsBasePermissionToGraphql,
};

export default mailingCampaignsPermissionToGraphql;
