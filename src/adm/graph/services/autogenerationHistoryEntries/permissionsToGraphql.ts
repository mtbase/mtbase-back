import autogenerationHistoryEntriesBasePermissionToGraphql from './basePermissionsToGraphql';
import {AutogenerationHistoryEntriesService} from '../../../services/AutogenerationHistoryEntriesService/AutogenerationHistoryEntriesService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const autogenerationHistoryEntriesPermissionToGraphql: Partial<PermissionToGraphql<AutogenerationHistoryEntriesService>> = {
  ...autogenerationHistoryEntriesBasePermissionToGraphql,
};

export default autogenerationHistoryEntriesPermissionToGraphql;
