import mailingMessageStatusesBasePermissionToGraphql from './basePermissionsToGraphql';
import {MailingMessageStatusesService} from '../../../services/MailingMessageStatusesService/MailingMessageStatusesService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const mailingMessageStatusesPermissionToGraphql: Partial<PermissionToGraphql<MailingMessageStatusesService>> = {
  ...mailingMessageStatusesBasePermissionToGraphql,
};

export default mailingMessageStatusesPermissionToGraphql;
