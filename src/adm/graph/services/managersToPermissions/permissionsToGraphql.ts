import managersToPermissionsBasePermissionToGraphql from './basePermissionsToGraphql';
import {ManagersToPermissionsService} from '../../../services/ManagersToPermissionsService/ManagersToPermissionsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const managersToPermissionsPermissionToGraphql: Partial<PermissionToGraphql<ManagersToPermissionsService>> = {
  ...managersToPermissionsBasePermissionToGraphql,
};

export default managersToPermissionsPermissionToGraphql;
