import messageTemplatesBasePermissionToGraphql from './basePermissionsToGraphql';
import {MessageTemplatesService} from '../../../services/MessageTemplatesService/MessageTemplatesService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const messageTemplatesPermissionToGraphql: Partial<PermissionToGraphql<MessageTemplatesService>> = {
  ...messageTemplatesBasePermissionToGraphql,
};

export default messageTemplatesPermissionToGraphql;
