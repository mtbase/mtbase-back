/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Permission,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: Permission,
): Promise<void> => {};
