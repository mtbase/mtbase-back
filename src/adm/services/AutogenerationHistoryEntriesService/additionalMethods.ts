import {Context} from '../types';
import {BaseAutogenerationHistoryEntriesMethods} from './AutogenerationHistoryEntriesService';

export interface AdditionalAutogenerationHistoryEntriesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAutogenerationHistoryEntriesMethods): AdditionalAutogenerationHistoryEntriesMethods => ({});
