import {
  MutationRemoveAutogenerationHistoryEntryArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const beforeDelete = async (
  _ctx: Context,
  _params: MutationRemoveAutogenerationHistoryEntryArgs,
// eslint-disable-next-line @typescript-eslint/no-empty-function
): Promise<void> => {};
