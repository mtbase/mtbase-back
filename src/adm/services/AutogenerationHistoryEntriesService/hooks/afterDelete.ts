/* eslint-disable @typescript-eslint/no-empty-function */
import {
  AutogenerationHistoryEntry,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterDelete = async (
  _ctx: Context,
  _data: AutogenerationHistoryEntry,
): Promise<void> => {};
