import {Context} from '../types';
import {BaseAggregateTrackingsMethods} from './AggregateTrackingsService';

export interface AdditionalAggregateTrackingsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAggregateTrackingsMethods): AdditionalAggregateTrackingsMethods => ({});
