/* eslint-disable @typescript-eslint/no-empty-function */
import {
  AggregateTracking,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterDelete = async (
  _ctx: Context,
  _data: AggregateTracking,
): Promise<void> => {};
