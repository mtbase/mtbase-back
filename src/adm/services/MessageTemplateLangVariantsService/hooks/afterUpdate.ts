/* eslint-disable @typescript-eslint/no-empty-function */
import {
  MessageTemplateLangVariant,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: MessageTemplateLangVariant,
): Promise<void> => {};
