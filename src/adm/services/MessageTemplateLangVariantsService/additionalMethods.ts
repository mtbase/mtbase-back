import {Context} from '../types';
import {BaseMessageTemplateLangVariantsMethods} from './MessageTemplateLangVariantsService';

export interface AdditionalMessageTemplateLangVariantsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseMessageTemplateLangVariantsMethods): AdditionalMessageTemplateLangVariantsMethods => ({});
