import {Context} from '../types';
import {BaseTenantsMethods} from './TenantsService';

export interface AdditionalTenantsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseTenantsMethods): AdditionalTenantsMethods => ({});
