/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Tenant,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: Tenant,
): Promise<void> => {};
