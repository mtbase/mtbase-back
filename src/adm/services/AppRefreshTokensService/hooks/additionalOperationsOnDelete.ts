import {
  MutationRemoveAppRefreshTokenArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const additionalOperationsOnDelete = async (
  _ctx: Context,
  _data: MutationRemoveAppRefreshTokenArgs,
) => [];
