import {Context} from '../types';
import {BaseFilesMethods} from './FilesService';

export interface AdditionalFilesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseFilesMethods): AdditionalFilesMethods => ({});
