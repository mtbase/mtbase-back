/* eslint-disable @typescript-eslint/no-empty-function */
import {
  File,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterDelete = async (
  _ctx: Context,
  _data: File,
): Promise<void> => {};
