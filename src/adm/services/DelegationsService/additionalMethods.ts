import {Context} from '../types';
import {BaseDelegationsMethods} from './DelegationsService';

export interface AdditionalDelegationsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseDelegationsMethods): AdditionalDelegationsMethods => ({});
