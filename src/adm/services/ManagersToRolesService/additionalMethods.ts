import {Context} from '../types';
import {BaseManagersToRolesMethods} from './ManagersToRolesService';

export interface AdditionalManagersToRolesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseManagersToRolesMethods): AdditionalManagersToRolesMethods => ({});
