/* eslint-disable @typescript-eslint/no-empty-function */
import {
  ManagersToRole,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterDelete = async (
  _ctx: Context,
  _data: ManagersToRole,
): Promise<void> => {};
