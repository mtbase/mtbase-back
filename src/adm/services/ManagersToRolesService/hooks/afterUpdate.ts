/* eslint-disable @typescript-eslint/no-empty-function */
import {
  ManagersToRole,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: ManagersToRole,
): Promise<void> => {};
