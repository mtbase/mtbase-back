import {
  MutationRemoveTagArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const beforeDelete = async (
  _ctx: Context,
  _params: MutationRemoveTagArgs,
// eslint-disable-next-line @typescript-eslint/no-empty-function
): Promise<void> => {};
