import {Context} from '../types';
import {BaseMailingMessagesMethods} from './MailingMessagesService';

export interface AdditionalMailingMessagesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseMailingMessagesMethods): AdditionalMailingMessagesMethods => ({});
