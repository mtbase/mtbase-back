import {Context} from '../types';
import {BaseManagersToPermissionsMethods} from './ManagersToPermissionsService';

export interface AdditionalManagersToPermissionsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseManagersToPermissionsMethods): AdditionalManagersToPermissionsMethods => ({});
