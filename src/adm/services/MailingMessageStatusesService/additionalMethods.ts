import {Context} from '../types';
import {BaseMailingMessageStatusesMethods} from './MailingMessageStatusesService';

export interface AdditionalMailingMessageStatusesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseMailingMessageStatusesMethods): AdditionalMailingMessageStatusesMethods => ({});
