/* eslint-disable @typescript-eslint/no-empty-function */
import {
  RolesToPermission,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterCreate = async (
  _ctx: Context,
  _data: RolesToPermission,
): Promise<void> => {};
