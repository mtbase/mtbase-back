import {Context} from '../types';
import {BaseAdmRefreshTokensMethods} from './AdmRefreshTokensService';

export interface AdditionalAdmRefreshTokensMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAdmRefreshTokensMethods): AdditionalAdmRefreshTokensMethods => ({});
