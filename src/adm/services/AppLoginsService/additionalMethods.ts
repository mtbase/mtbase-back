import {Context} from '../types';
import {BaseAppLoginsMethods} from './AppLoginsService';

export interface AdditionalAppLoginsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAppLoginsMethods): AdditionalAppLoginsMethods => ({});
