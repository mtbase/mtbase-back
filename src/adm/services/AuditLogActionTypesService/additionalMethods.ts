import {Context} from '../types';
import {BaseAuditLogActionTypesMethods} from './AuditLogActionTypesService';

export interface AdditionalAuditLogActionTypesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAuditLogActionTypesMethods): AdditionalAuditLogActionTypesMethods => ({});
