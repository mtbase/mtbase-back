import {
  MutationRemoveAutogenerationRuleArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const beforeDelete = async (
  _ctx: Context,
  _params: MutationRemoveAutogenerationRuleArgs,
// eslint-disable-next-line @typescript-eslint/no-empty-function
): Promise<void> => {};
