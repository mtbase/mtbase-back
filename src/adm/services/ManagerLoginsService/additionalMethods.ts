import {Context} from '../types';
import {BaseManagerLoginsMethods} from './ManagerLoginsService';

export interface AdditionalManagerLoginsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseManagerLoginsMethods): AdditionalManagerLoginsMethods => ({});
