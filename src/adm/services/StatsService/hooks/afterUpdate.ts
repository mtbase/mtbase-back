/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Stat,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: Stat,
): Promise<void> => {};
