import {Context} from '../types';
import {BaseUnitsMethods} from './UnitsService';

export interface AdditionalUnitsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseUnitsMethods): AdditionalUnitsMethods => ({});
