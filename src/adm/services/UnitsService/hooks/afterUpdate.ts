/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Unit,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: Unit,
): Promise<void> => {};
